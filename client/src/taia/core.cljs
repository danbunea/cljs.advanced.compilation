(ns taia.core
  (:require
   [reagent.dom :as r]
   [datascript.core :as d]
   [re-posh.core :as re-posh]
   
   ))


(def taia-schema
  {:app/id {:db.unique :db.unique/identity}
   :app/name {}
   :app/type {}})


(def initial-db
  [{:app/id :bunea.projects
    :app/name "Projects"}
   {:app/id :bunea.mudanza
    :app/name "Mudanza"}
   {:app/id :bunea.deuras-alex
    :app/name "Deuras Alex Verano 2021"}])


(def conn (d/create-conn taia-schema))
(d/transact! conn initial-db)
(re-posh/connect! conn)

(defn home-page []
  [:span "Hola"])

;; -------------------------
;; Initialize app
(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))


  
(defn ^:export init! []
  (->>
   (d/q '[:find ?e ?a ?v
           :where
           [?e ?a ?v]
           ]
         @conn)
   (println :db))
  (mount-root))
    
    
    
    


